import discord
from discord.ext import commands
import random
import time
import os
import asyncio
import nest_asyncio

nest_asyncio.apply()

def list_save(filename) :
	with open(filename, "r") as f :
		fread, char, contact, liste = f.read(), [], [], []
	for c in fread :
		if c == " " :
			contact.append("".join(char))
			char = []
		elif c == "\n" :
			contact.append("".join(char))
			liste.append(contact)
			char, contact = [], []
		else :
			char.append(c)
	return liste

def printer(filename, chan) :
	new, count = "", 0
	for i in chan :
		for j in i :
			count += 1
			new += str(j)
			if count != len(i) :
				new += " "
		count = 0
		new += "\n"
	with open(filename, "w") as f :
		f.write(new)


with open("token.txt", "r") as token:
	TOKEN = token.read()

bot = commands.Bot(command_prefix = "l-")
bot.remove_command('help')

@bot.event
async def on_ready():
	print('The bot is online')

@bot.group()
async def help(ctx) :

	embed=discord.Embed(title="Commands to Help you :)", color=0xad10e9)
	embed.set_author(name="ladbot", icon_url="https://cdn.discordapp.com/attachments/707531304165572628/760176244842299442/Mangaku.jpg")
	embed.set_thumbnail(url="https://cdn.discordapp.com/attachments/707531304165572628/760176244842299442/Mangaku.jpg")
	embed.add_field(name="-help", value="Montre ce menu", inline=True)
	embed.add_field(name="-ping", value="Renvoie pong", inline=True)
	embed.add_field(name="-8ball", value="Ask a question :)", inline=True)
	embed.add_field(name="-here", value="Pour être sur que je suis la.", inline=True)
	embed.add_field(name="-info", value="Pour plus d'info sur un utilisateur en particulier", inline=True)
	embed.add_field(name="-whoami", value="Pour voir si j'ai le rôle Admin (HS)", inline=True)
	embed.add_field(name="-kick", value="Pour kick les malotrus", inline=True)
	embed.add_field(name="-ban", value="Pour Bannir les erreurs", inline=True)
	embed.add_field(name="-unban", value="Pour leur donner une seconde chance", inline=True)
	embed.add_field(name="-mute", value="Pour faire taire les idiots", inline=True)
	embed.add_field(name="-unmute", value="Pour voir si ils ont regagné en maturité", inline=True)
	embed.add_field(name="-channel", value="Pour selectionner un salon", inline=True)
	embed.add_field(name="-message", value="Pour envoyer un message dans le salon sélectionner", inline=True)
	embed.add_field(name="-edit", value="Pour éditer un précédent message", inline=True)
	embed.add_field(name="-autorole", value="Pour ajouter un système d'autorole", inline=True)
	embed.set_footer(text="Ce bot est encore en phase de développement.")

	await ctx.send(embed = embed)

@help.command()
async def autorole(ctx) :
    embed=discord.Embed(title="Autorole :", color=0xb70101)
    embed.set_author(name="Ladbot", icon_url="https://cdn.discordapp.com/attachments/707531304165572628/760176244842299442/Mangaku.jpg")
    embed.set_thumbnail(url="https://cdn.discordapp.com/attachments/707531304165572628/760176244842299442/Mangaku.jpg")
    embed.add_field(name="show roles", value="-autorole", inline=True)
    embed.add_field(name="add roles", value="-autorole add {@role}", inline=True)
    embed.add_field(name="delete 1 role", value="-autorole delete {@role}", inline=True)
    embed.add_field(name="clear roles", value="-autorole clear", inline=True)
    embed.set_footer(text="Ce bot est encore en phase de développement.")
    await ctx.send(embed = embed)

@bot.command()
@commands.has_permissions(manage_messages = True)
async def clear(ctx, amount = 3):
	await ctx.channel.purge(limit = amount + 1)
	await ctx.send(f'{amount} messages have been cleared !')
	await asyncio.sleep(1.5)
	await ctx.channel.purge(limit = 1)

@clear.error
async def clear_error(error, ctx):
	pass

@bot.command()
async def ping(ctx):
	await ctx.send(f'pong ! {round(bot.latency * 1000)}ms')

@bot.command(aliases = ["8ball"])
async def _8ball(ctx, *, question):
	responses = ["As I see it, yes.",
				 "Ask again later.",
				 "Better not tell you now.",
				 "Cannot predict now.",
				 "Concentrate and ask again.",
				 "Don’t count on it.",
				 "It is certain.",
				 "It is decidedly so.",
				 "Most likely.",
				 "My reply is no.",
				 "My sources say no.",
				 "Outlook not so good.",
				 "Outlook good.",
				 "Reply hazy, try again.",
				 "Signs point to yes.",
				 "Very doubtful.",
				 "Without a doubt.",
				 "Yes.",
				 "Yes – definitely.",
				 "You may rely on it."]
	await ctx.send(f"Answer : {random.choice(responses)}")

@bot.command()
async def here(ctx) :
	await ctx.send("Je suis la ^^")
	
@bot.command()
@commands.has_permissions(kick_members=True)
async def kick(ctx, member : discord.Member, *, reason = None):
	await member.kick(reason = reason)
	if reason == None :
		await ctx.send(f"{member.mention} has been kicked !")
	else :
		await ctx.send(f"{member.mention} has been kicked because {reason}...")

@kick.error
async def kick_error(error, ctx):
   if isinstance(error, MissingPermissions(missing)):
	   await ctx.send("Dsl tu n'a pas la permission de faire ça :/")

@bot.command()
@commands.has_permissions(ban_members=True)
async def ban(ctx, member : discord.Member, *, reason = None):
	await member.ban(reason = reason)
	if reason == None :
		await ctx.send(f"{member.mention} has been banned !")
	else :
		await ctx.send(f"{member.mention} has been banned because {reason}")

#@bot.command()
#async def unban(ctx, *, member):

#	banned_users = await ctx.guild.bans()
#	member_name, member_discriminator = member.split("#")
	
#	for ban_entry in banned_users:
#		user = ban_entry.user

#		if (user.name, user.discriminator) == (member_name, member_discriminator):
#			await ctx.guild.unban(user)
#			await ctx.send(f"Nice, vous venez de unban {user.mention}")
#			return

@bot.command(name='unban')
@commands.has_permissions(ban_members=True)
async def _unban(ctx, id: int):
	user = await bot.fetch_user(id)
	await ctx.guild.unban(user)
	await ctx.send(f"le joueur avec l'id {id} a bien été unban :)")

@bot.command()
@commands.has_permissions(manage_messages=True)
async def mute(ctx, member : discord.Member, *,reason = None) :
	guild = ctx.guild

	for role in guild.roles :
		if role.name == "Muted":
			await member.add_roles(role)
			if reason == None :
				await ctx.send(f"Mmmm, {member.mention} viens d'être mute par {ctx.author.mention}...\nRepasse plus tard !")
			else :
				await ctx.send(f'Mmmm, {member.mention} viens d\'être mute par {ctx.author.mention} pour la raison suivante : "{reason}"...\nRepasse plus tard !')
			return


	overwrite = discord.PermissionOverwrite(send_messages = False)
	newRole = await guild.create_role(name="Muted")

	for channel in guild.text_channels :
		await channel.set_permissions(newRole,overwrite=overwrite)
		print(channel)

	await member.add_roles(newRole)
	if reason == None :
		await ctx.send(f"Mmmm, {member.mention} viens d'être mute par {ctx.author.mention}...\nRepasse plus tard !")
	else :
		await ctx.send(f'Mmmm, {member.mention} viens d\'être mute par {ctx.author.mention} pour la raison suivante : "{reason}"...\nRepasse plus tard !')
	return

@bot.command()
@commands.has_permissions(manage_messages=True)
async def unmute(ctx, member : discord.Member, *, reason = None) :
	guild = ctx.guild

	for role in guild.roles :
		if role.name == "Muted":
			await member.remove_roles(role)
			if reason == None :
				await ctx.send(f"Tiens ! {member.mention} a été unmute par {ctx.author.mention} !")
			else :
				await ctx.send(f'Tiens ! {member.mention} a été unmute par {ctx.author.mention} pour la raison "{reason}" !')
			return

@bot.command()
async def whoami(ctx) :
	if ctx.message.author.has_permissions(administrator = True) :
		await ctx.send("Bravo tu est un admin !")
	else :
		await ctx.send("Nope tu n'est pas admin :(")

@bot.command()
async def info(ctx, *, member : discord.Member):
	await ctx.send(f"{member} has {len(member.roles)} roles.")

@bot.command()
@commands.has_permissions(manage_messages=True)
async def channel(ctx, channel : discord.TextChannel) :
	channel_id, guild_id, chan, new_guild = channel.id, ctx.guild.id, list_save("saved_channels.csv"), True
	for i in chan :
		if i[0] == str(guild_id) :
			i[1] = channel_id
			new_guild = False
	if new_guild == True :
		with open("saved_channels.csv", "a") as f :
			f.write(f"{guild_id} {channel_id}\n")
	else :
		printer("saved_channels.csv", chan)
	embed=discord.Embed(color=0x00ca1f)
	embed.add_field(name = f"Le channel a bien été selectionné !", value = f"Le channel est maintenant : {channel.mention}", inline=False)
	await ctx.send(embed=embed)

@bot.command()
@commands.has_permissions(manage_messages=True)
async def message(ctx, *, message) :
	chan, guild = list_save("saved_channels.csv"), ctx.guild.id
	for i in chan :
		if str(guild) == i[0] :
			channel = bot.get_channel(int(i[1]))
			await channel.send(message)
			break
	embed=discord.Embed(color=0x00ca1f)
	embed.add_field(name="Success :white_check_mark:", value=f"Le message a bien été envoyé dans {channel.mention}", inline=False)
	await ctx.send(embed = embed)

@bot.command()
@commands.has_permissions(manage_messages=True)
async def edit(ctx, message_id, *, new_message) :
	channel, chan, guild = None, list_save("saved_channels.csv"), ctx.guild.id
	for i in chan :
		if str(guild) == i[0] :
			channel = bot.get_channel(int(i[1]))
			message = await channel.fetch_message(message_id)
			old_message = message.content
			await message.edit(content = new_message)
			break
	embed=discord.Embed(color=0x00ca1f)
	embed.add_field(name="Success :white_check_mark:", value=f"Le message a bien été édité dans {channel.mention}", inline=False)
	await ctx.send(embed = embed)

@bot.group()
@commands.has_permissions(manage_roles=True)
async def autorole(ctx) :
	save, new_guild, stop, ladrole = list_save("autorole.csv"), True, False, ""
	for i in save :
		if i[0] == str(ctx.guild.id) :
			new_guild = False
	if new_guild == True :
		with open ("autorole.csv", "a") as f :
			f.write(f"{guild_id}\n")
	roles_id, new_ids = [], []
	for i in save :
		if str(ctx.guild.id) == i[0] :
			for r in ctx.guild.roles[1:] :
				if r.name == "ladbot" :
					ladrole = r
					stop = True
				elif str(r.id) in i[1:] :
					if stop == False :
						new_ids.append(r.id)
					if stop == True :
						await ctx.send(f"Je ne peux pas attribuer le role {r.mention} car le role {ladrole.mention} est situé en dessous de celui-ci :/")
			del i[1:]
			i += new_ids
			printer("autorole.csv", save)
	if ctx.invoked_subcommand is None:
		save = list_save("autorole.csv")
		for i in save :
			if str(ctx.guild.id) == i[0] :
				embed=discord.Embed(color=0x00ca1f, title="Voici la liste des roles automatiques :")
				count = 1
				for j in i[1:] :
					role = ctx.guild.get_role(int(j))
					embed.add_field(name=f"Role {count} :", value=f"{role.mention}", inline=False)
					count += 1
				if count == 1 :
					embed.add_field(name="Vous n'avez pas encore d'autorole programmé", value="Essayez de faire -autorole add @role", inline=False)
				
				embed.set_footer(text="Ces roles seront attribués a tous les nouveaux arrivants.")
				await ctx.send(embed = embed)

@autorole.command()
@commands.has_permissions(manage_roles=True)
async def add(ctx, new_role : discord.Role) :
	save, guild, ladrole = list_save("autorole.csv"), ctx.guild.id, ""
	for i in save :
		if str(guild) == i[0] :

			i.append(new_role.id)
	printer("autorole.csv", save)
	await ctx.send(f"Le role {new_role.mention} a bien été ajouté.")
	roles_id, new_ids, save, stop = [], [], list_save("autorole.csv"), False
	for i in save :
		if str(ctx.guild.id) == i[0] :
			for r in ctx.guild.roles[1:] :
				if r.name == "ladbot" :
					ladrole = r
					stop = True
				elif str(r.id) in i[1:] :
					if stop == False :
						new_ids.append(r.id)
					if stop == True :
						await ctx.send(f"Je ne peux pas attribuer le role {r.mention} car le role {ladrole.mention} est situé en dessous de celui-ci :/")
			del i[1:]
			i += new_ids
			printer("autorole.csv", save)


@autorole.command()
@commands.has_permissions(manage_roles=True)
async def delete(ctx, del_role : discord.Role) :
	chan, guild = list_save("autorole.csv"), ctx.guild.id
	for i in chan :
		if str(guild) == i[0] :
			i.remove(str(del_role.id))
			printer("autorole.csv", chan)
			await ctx.send(f"Le role {del_role.mention} a bien été enlevé !")

@autorole.command(aliases = ["clear"])
@commands.has_permissions(manage_roles=True)
async def delete_all(ctx) :
	chan, guild = list_save("autorole.csv"), ctx.guild.id
	for i in chan :
		if str(guild) == i[0] :
			del i[1:]
			printer("autorole.csv", chan)
			await ctx.send(f"Tous les roles ont bien étés enlevés !")

@bot.event
async def on_member_join(member) :
	save, new_guild, guild = list_save("autorole.csv"), True, member.guild
	for i in save :
		if i[0] == str(guild.id) :
			new_guild = False
	if new_guild == True :
		with open ("autorole.csv", "a") as f :
			f.write(f"{guild_id}\n")
	roles_id, new_ids, save = [], [], list_save("autorole.csv")
	for i in save :
		if str(guild.id) == i[0] :
			for r in guild.roles[1:] :
				roles_id.append(r.id)
			for id in roles_id :
				if str(id) in i[1:] :
					new_ids.append(id)
			del i[1:]
			i += new_ids
			printer("autorole.csv", save)
	save = list_save("autorole.csv")
	for i in save :
		if str(guild.id) == i[0] :
			for j in i[1:] :
				role = member.guild.get_role(int(j))
				await member.add_roles(role)

@bot.command()
async def file(ctx, filename = None) :
	if filename == None :
		files = [discord.File("autorole.csv"), discord.File("saved_channels.csv")]
	else :
		files = [discord.File(filename)]
	await ctx.send(files=files)

@bot.command()
async def roulette(ctx, *, members) :
	list = members.split(" ")
	for i in list :
		await ctx.send(i)
		print(i)


@bot.group()
async def join(ctx) :
	save, new_guild, stop, ladrole = list_save("join_leave.csv"), True, False, ""
	for i in save :
		if i[0] == str(ctx.guild.id) :
			new_guild = False
	if new_guild == True :
		with open ("join_leave.csv", "a") as f :
			f.write(f"{guild_id} off off Hey_")
	save = list_save("join_leave.csv")


@bot.command()
async def eureka(ctx) :
	await ctx.send("YASS BITCHES")
    

@info.error
async def info_error(ctx, error):
	if isinstance(error, commands.BadArgument):
		await ctx.send("I couldn't find that member, sorry...")

bot.run(TOKEN)